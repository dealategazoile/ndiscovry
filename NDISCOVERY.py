
import threading
from queue import Queue
import sys
import getopt
from host import Host
from ping_on_network import *
from ip import *
from write_the_result import *
import datetime

arguments = sys.argv[1:]

optlist, args = getopt.getopt(arguments, 'n:r::t')


addresses_queue= Queue()
list_adrr_up2= []
thread_queue = []
def fill_addresses_queue(network_entred="192.168.1.",start_rang=1,end_rang=255):
    for i in range(start_rang, end_rang):
        addresses_queue.put(network_entred+str(i))

def ping_from_the_queue():
    while not addresses_queue.empty():
        addr= addresses_queue.get()
        if ping(addr):
            print( addr +" is up ************")
            list_adrr_up2.append(Host(addr))



def ping_network_with_multithreading(network_entred="192.168.1.",start_rang=1 , end_rang=254 ,number_of_threads=254):
    network=network_entred
    if(not verification_of_semiIp(network_entred)):
        if(verification_of_ip(network_entred)):
            network = arrangement_of_ip_to_semi_ip(network_entred)
           # print(network)
          #  return
        else :
            return
    fill_addresses_queue(network ,start_rang,end_rang)
    for i in range(number_of_threads):
        thread = threading.Thread(target=ping_from_the_queue)
        thread_queue.append(thread)
    for thread in thread_queue:
        thread.start()
    for thread in thread_queue:
        thread.join()
        
    separation = "\n****************************************************\n"
    print(separation + "NDISCOVRY BY DELATEGA"+separation)
    time = datetime.datetime.now()
    print("hosts up on the network "+ network+str(0)+"/24 on "+str(time)+"  are :")    
        
    for h in list_adrr_up2:
        print (h.to_string())
    write_the_result(network , list_adrr_up2, time)



def get_parametres():
    arguments = sys.argv[1:]
    optlist, args = getopt.getopt(arguments, 'n:r:th')
    network  =arrangement_of_ip_to_semi_ip(get_ip_addr())

    start_rang = 1
    end_rang = 255

    for o, a in optlist:
        if o in ("-n"):
                if(not verification_of_semiIp(a)):
                    if(verification_of_ip(a)):
                        network = arrangement_of_ip_to_semi_ip(a) 
                    else :
                        exit(1)
                network= a
        elif o in ("-r"):
            try:
                the_ranges=a.split(',')
                start_rang = int(the_ranges[0])
                end_rang = int(the_ranges[1])
            except:
                print("-r define the range of addresses to ping and must be in the spesifique formate 'srat_range,end_range'")
                print("ex : -r 1,254 ")  
                exit(1)
        elif o in ("-h"):
            print ("the script must run with the this syntax : ")
            print ("python ping_addr.py -n 192.168.1. -r 1,254")  
            return      
        else:
            assert False, "unhandled option tape 'python ping_addr.py -h'"
    return [network,start_rang,end_rang]
parametres = get_parametres()
ping_network_with_multithreading(parametres[0] , parametres[1] , parametres[2])

hostname = socket.gethostname()
## getting the IP address using socket.gethostbyname() method
#ip_address = socket.gethostbyname(hostname)
## printing the hostname and ip_address
#ip_address = socket.gethostbyname(hostname + ".local")
##ip_address = socket.gethostbyname(socket.gethostname())
##print(f"Hostname: {hostname}")
##print(f"IP Address: {ip_address}")

