from getmac import get_mac_address

def get_the_mac_adress(ip_add):

    ip_mac = get_mac_address(ip=ip_add)

    print (ip_mac)

    return ip_mac

def get_the_mac_adresses(ip_adds):
    MACs = []
    for ip_add in ip_adds:
        ip_mac = get_mac_address(ip=ip_add)
        MACs.append(ip_add+" "+str(ip_mac))
    return MACs

    

get_the_mac_adress("192.168.1.1")
get_the_mac_adresses(["192.168.1.1","192.168.1.2"])
