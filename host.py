#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 27 11:39:55 2021

@author: dairakolea
"""
from getmac import get_mac_address
from read_from_file import *


class Host:
    
    def __init__(self, ip):
        self.ip = ip
        self.mac = get_mac_address(ip=ip)
        self.name=""
        self.adaptater=""
        self.verifyed=""
        self.ok=""
        self.deailled=rechercher_by_mac(self.mac, ip)
        
        
    def to_string(self):
        separation = "\n******************************************************\n"
        return separation + "ip = "+ str(self.ip) +"\n"+ "mac = " + str(self.mac) +"\n" + "detail : \n" + str(self.deailled)