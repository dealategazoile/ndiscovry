#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 29 14:41:30 2021

@author: dairakolea
"""
import socket
import re 


def verification_of_ip(ip_address):
    if (not re.search("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", ip_address)):
        print("l adress "+ ip_address+" ip entrer n est pas valide ")
        print("l adress doit etre x.x.x.x ex : 192.168.1.1")
        return False
    print ("ip verifyed !!!")
    return True


def verification_of_semiIp(semi_ip_address):
    if (re.search("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}$", semi_ip_address)):
        print ("semi ip verifyed !!!")
        return True
    

def arrangement_of_ip_to_semi_ip(ip_address):
    network = ".".join(ip_address.split(".", 3)[:3])+"."
    return network


def get_ip_addr():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]
    s.close()
    return ip

