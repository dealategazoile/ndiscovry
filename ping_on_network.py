import platform    # For getting the operating system name
import subprocess  # For executing a shell command


def ping(host):
    """
    Returns True if host (str) responds to a ping request.
    Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
    """

    # Option for the number of packets as a function of
    param = '-n' if platform.system().lower()=='windows' else '-c'

    # Building the command. Ex: "ping -c 1 google.com"
    command = ['ping', param, '1', host]

    return subprocess.call(command) == 0

    
def ping_network(network_entred , start_rang=1 , end_rang=255):    


    
    if (start_rang <= 0):
        print ("le range de debut doit etre superieur a 1 ce qui n'est pas le cas de "+ str(start_rang))
        return False

    if (end_rang >= 255):
        print ("le range de fin doit etre inferieur a 255 ce qui n'est pas le cas de "+ str(end_rang))
        return False
    

    #x = re.search("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)?$", network)
    if (not re.search("^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$", network_entred)):
        print("l adress "+ network_entred+" ip entrer n est pas valide ")
        print("l adress doit etre x.x.x.x ex : 192.168.1.1")
        return False
    network = ".".join(network_entred.split(".", 3)[:3])
    
    list_adrr_up = []

    for i in range(start_rang,end_rang):

        host = network+"."+str(i)

        if (ping (host)):

            list_adrr_up.append(host)
            print ("******************up "+host+"*****************")


    return list_adrr_up  