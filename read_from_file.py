#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 28 10:27:11 2021

@author: dairakolea
"""

import json


def rechercher_by_mac(mac_addr , network):
    network_name_file=network.replace('.','_')
    network_name_file=network_name_file[:3]
    try:
        with open('mac_add_'+network_name_file+'.json', 'r') as json_file:
                data = json.load(json_file)
                if str(mac_addr).lower() in data :
                    return data[str(mac_addr).lower()]
                
                if  str(mac_addr).upper() in data  :
                    return data[str(mac_addr).upper()]
        
                 
        add_new_mac_on_file(data, network_name_file, mac_addr )    
        return "hote inconnue (detected for the first time)"
    except :
        data = {}
        data["mac"]={}
        add_new_mac_on_file(data, network_name_file, mac_addr )    


def add_new_mac_on_file(data , network_name_file , mac_addr , name="",adaptater="",verifyed="" , authorised = ""):
    data_mac = data ["mac"]
    data_mac[str(mac_addr)]={"name": name,"adaptater": adaptater,"verifiyed": verifyed ,"authorised": authorised}
    with open('mac_add_'+network_name_file+'.json', "w") as f_out:
        json.dump(data , f_out)



rechercher_by_mac("04:EA:36:00:21:40aa","192.168.1.")