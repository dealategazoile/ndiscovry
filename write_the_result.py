#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  3 00:51:43 2021

@author: delatega
"""
separation = "\n****************************************************\n"

def write_the_result(network , list_adrr_up2, time):
    network_name_file=network.replace('.','_')
    f = open("result_"+network_name_file+"0.txt", "r+")
    f.write(separation+"NDISCOVER BY DELATEGA"+separation)
    f.write("hosts up on the network "+ network+str(0)+"/24 on "+str(time)+"  are :")
    for h in list_adrr_up2:
        f.write(h.to_string())
    f.close()